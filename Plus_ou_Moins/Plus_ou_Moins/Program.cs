﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Plus_ou_Moins
{
    class Program
    {
        static void Main(string[] args)
        {
            //PlusMoinsMachineVSHuman();
            SortArrays();
        }

        public static void PlusMoinsMachineVSHuman()
        {
            Console.WriteLine("Choisissez un nombre a faire deviner à l'ordinateur entre 1 à 100"
                + "\nL'ordinateur tentera de deviner en un minimum de coup votre nombre" 
                + "\n\n A chaque proposition saisissez un \"+\" si c'est plus ou un \"-\" si c'est moins"
                + "ou un = si c'est le bon nombre");

            Random rand = new Random();
            int choice = rand.Next(0, 100);
            String indic = "";
            int max = 100, min = 0;
            do
            {
                Console.WriteLine("-> " + choice);
                Console.Write("Plus ou moins ? : ");
                indic = Console.ReadLine();
                if (indic.Equals("+"))
                {
                    min = min < choice ? choice : min;
                   
                }
                else if (indic.Equals("-"))
                {
                    max = max > choice ? choice : max;
                }
                else
                {
                    Console.WriteLine("Bad character");
                }
                choice = min + ( (max - min) / 2);
            } while (!indic.Equals("="));
        }

        public static void PlusMoinsHumanVSMachine()
        {
            Random rand = new Random();
            int response = -1;
            int number = rand.Next(0, 100);
            while (response != number)
            {
                Console.WriteLine("Saississez un nombre entier entre 1 et 100");
                String input = Console.ReadLine();
                int value;
                if (int.TryParse(input, out value))
                {
                    if (value < number)
                    {
                        Console.WriteLine("C'est plus ...");
                    }
                    else if (value > number)
                    {
                        Console.WriteLine("C'est moins ...");
                    }
                    else
                    {
                        Console.WriteLine("Gagné !!! Le nombre était : " + number +
                            "\n Appuyez sur une touche pour quitter.");
                        Console.ReadLine();
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("Le nombre saisie n'est pas un entier");
                }
            }
        }

        public static void CheckMyVoyelles()
        {
            String sentence = "";
            do
            {
                Console.WriteLine("Saisissez une phrase : ");
                sentence = Console.ReadLine();
                Regex regex = new Regex(@"a|e|i|o|u|i|é|è|ê|y");
                MatchCollection match = regex.Matches(sentence.ToLower());
                Console.WriteLine("Nombre de voyelles : " + match.Count);
            } while (!sentence.Equals(""));
        }

        public static void SortArrays()
        {
            bool ok = false;
            int taille;
            do {
                Console.Write("Saisissez la taille des tableau à saisir : " + "\nTaille :");
                ok = int.TryParse(Console.ReadLine(), out taille);
            } while (!ok);

            Console.WriteLine("Saisissez un à un " + taille*2 + " nombres entiers" +
                "\nles " + taille + " premiers seront stockés dans un premier tableau " +
                "\nles " + taille + " suivants dans un second");
            int[] tab1 = new int[taille];
            int[] tab2 = new int[taille];
            int[] tab3 = new int[taille];
            Console.WriteLine("Permier tableau :");
            for (int i = 0; i < taille; i++)
            {
                int saisie;
                ok = int.TryParse(Console.ReadLine(), out saisie);
                if (ok)
                {
                    tab1[i] = saisie;
                }else
                {
                    Console.WriteLine("Cecie n'est pas un entier, ressaisissez ...");
                    i--;
                }
            }
            Console.WriteLine("Second tableau :");
            for (int i = 0; i < taille; i++)
            {
                int saisie;
                ok = int.TryParse(Console.ReadLine(), out saisie);
                if (ok)
                {
                    tab2[i] = saisie;
                }
                else
                {
                    Console.WriteLine("Cecie n'est pas un entier, ressaisissez ...");
                    i--;
                }
            }
            Console.WriteLine("Ajout des valuers 2 à 2 dans un troisième tableau");
            for(int i = 0 ; i < taille ; i++)
            {
                tab3[i] = tab1[i] + tab2[i];
                Console.Write(tab3[i] + " | ");
            }
            bool oneMoreTime = true;
            do
            {
                for (int i = 0; i < taille; i++)
                {
                    for(int j = 1; j < taille; j++)
                    {

                    }
                }
            } while (true);
        }
    }
}
